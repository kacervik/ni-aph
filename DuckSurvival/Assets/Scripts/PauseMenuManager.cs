using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PauseMenuManager : MonoBehaviour {
    public static bool isGamePaused = false;
    public GameObject pauseMenu;
    public GameObject controlsMenu;
    public GameObject player;

    public GameObject pauseMenuFirstButton, controlsMenuFirstButton;


    void Start() {
        isGamePaused = false;
        pauseMenu.SetActive(false);
    }

    private void Update() {
        // get input to pause game
        if (player.GetComponent<playerInputManager>().pauseButton) {
            if (!isGamePaused) {
                pauseGame();
            }
        } else {
            if (isGamePaused) {
                resumeGame();
            }
        }
    }

    public void pauseGame() {
        isGamePaused=true;
        Time.timeScale = 0f;
        controlsMenu.SetActive(false);
        pauseMenu.SetActive(true);
        setPauseMenuFirstButton();
    }

    public void resumeGame() { 
        isGamePaused=false;
        player.GetComponent<playerInputManager>().pauseButton = false;
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);
        controlsMenu.SetActive(false);
    }

    public void returnToMainMenu() {
        Time.timeScale = 1f;
        pauseMenu.SetActive(false);
        controlsMenu.SetActive(false);
        SceneManager.LoadSceneAsync(0);
    }

    public void setPauseMenuFirstButton() {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(pauseMenuFirstButton);
    }

    public void setControlsMenuFirstButton() {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(controlsMenuFirstButton);
    }
}
