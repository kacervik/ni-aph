using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxEffect : MonoBehaviour {

    private float startPos = 0f;
    private GameObject cam = null;
    [SerializeField] private float parallaxDistance = 0f;

    // Start is called before the first frame update
    void Start() {
        cam = GameObject.Find("Main Camera");
        startPos = transform.position.x;
    }

    // Update is called once per frame
    void FixedUpdate() {
        float distance = cam.transform.position.x * parallaxDistance;
        transform.position = new Vector3(startPos + distance, transform.position.y, transform.position.z);
    }
}
