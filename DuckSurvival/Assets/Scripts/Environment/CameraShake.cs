using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraShake : MonoBehaviour {

    public static CameraShake instance;

    private CinemachineVirtualCamera _cam;

    private float timer = 0f;
    private CinemachineBasicMultiChannelPerlin _multiChannelPerlin;

    private void Awake() {
        instance = this;
        _cam = gameObject.GetComponent<CinemachineVirtualCamera>();
    }

    private void Start() {
        StopShake();
    }

    private void Update() {
        if (timer > 0f) {
            timer -= Time.deltaTime;

            if (timer <= 0f) {
                StopShake();
            }
        }

        float lerpSpeed = 5f; // Adjust the speed as needed
        Camera.main.transform.localRotation = Quaternion.Lerp(Camera.main.transform.localRotation, Quaternion.identity, lerpSpeed * Time.deltaTime);
    }

    public void ShakeCamera(float time, float magnitude) {
        if (instance != null) {
            _multiChannelPerlin = _cam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
            _multiChannelPerlin.m_AmplitudeGain = magnitude;
            timer = time;
        }
        
    }
        
    public void StopShake() {
        Debug.Log("Stopping");
        _multiChannelPerlin = _cam.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        _multiChannelPerlin.m_AmplitudeGain = 0f;
    }
}
