using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour {

    public static UIManager instance;


    [Header("Wave")]
    public TMP_Text waveText;
    private int wave = 0;

    [Header("Lives")]
    public TMP_Text livesText;
    private int lives = 3;
    public Image[] hearts;
    public Sprite heart;

    [Header("Explosion")]
    [SerializeField] private float explosionSusceptibility = 1f;
    [SerializeField] private Slider slider;

    private void Awake() {
        instance = this;
    }

    private void Start() {
        waveText.text = "WAVE:" + wave.ToString();
    }

    public void increaseWave() {
        wave++;
        waveText.text = "WAVE:" + wave.ToString();
    }

    public void decreaseLives() {
        lives--;
        updateLivesUI();
    }

    private void updateLivesUI() {
        for (int i = 0; i < hearts.Length; i++) {
            if (i < lives) {
                hearts[i].sprite = heart;
            } else {
                hearts[i].color = new Color(1f, 1f, 1f, 0f); ;
            }
        }
    }

    public void increaseExplosionSusceptibility() {
        explosionSusceptibility *= 1.12f;
        slider.value = explosionSusceptibility;
    }

    public float getExplosionSusceptibility() {
        return explosionSusceptibility;
    }

    public void resetExplosionSusceptibility() {
        explosionSusceptibility = 1f;
        slider.value = explosionSusceptibility;
    }
}
