using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waveSystem : MonoBehaviour {

    /* Handles spawning of enemies
     * waves increasing with some function to increase number of ducks
     * spawn points (or random range)
     * UI?
     * check for new wave to start
     */

    [Header("Enemy")]
    public GameObject enemyPrefab = null;
    private List<GameObject> enemies = new List<GameObject>();

    [Header("Wave")]
    [SerializeField] private int waveNumber = 0;
    [SerializeField] private int baseEnemyCount = 5;
    [SerializeField] private int enemyIncrement = 5;
    [SerializeField] private int totalEnemies;
    [SerializeField] private float enemySpawnRate = 2f;

    [Header("Spawn Point")]
    [SerializeField] private int spawnHorizontalRange = 6;
    [SerializeField] private int spawnHeight = 12;

    private void Start() {
        StartCoroutine(spawnWave());
    }

    void Update() {
        checkEnemyStatus();
    }

    private int calculateTotalEnemyCount() {
        return baseEnemyCount + (waveNumber * enemyIncrement);
    }

    IEnumerator spawnWave() {
        waveNumber++;
        UIManager.instance.increaseWave();
        totalEnemies = calculateTotalEnemyCount();

        for (int i = 0; i < totalEnemies; i++) {
            spawnEnemy();
            yield return new WaitForSeconds(1f/enemySpawnRate);
        }
    }

    private void spawnEnemy() {
        GameObject enemy = Instantiate(enemyPrefab, new Vector3(Random.Range(-spawnHorizontalRange, spawnHorizontalRange), spawnHeight, 0), Quaternion.identity);
        enemies.Add(enemy);
    }

    private void checkEnemyStatus() {
        enemies.RemoveAll(enemy => enemy == null);

        if (enemies.Count == 0) {
            // All enemies are defeated, start the next wave
            StartCoroutine(spawnWave());
        }
    }
}
