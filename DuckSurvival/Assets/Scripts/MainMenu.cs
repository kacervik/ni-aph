using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class MainMenu : MonoBehaviour {

    public GameObject mainMenuFirstButton, controlsMenuFirstButton;

    private void Awake() {
        setMainMenuFirstButton();
    }

    public void PlayGame() {
        SceneManager.LoadSceneAsync(1);
        Time.timeScale = 1f;
    }

    public void setMainMenuFirstButton() {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(mainMenuFirstButton);
    }

    public void setControlsMenuFirstButton() {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(controlsMenuFirstButton);
    }

    public void QuitGame() {
        Debug.Log("Quit");
        Application.Quit();
    }
}
