using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class enemyFollowTarget : MonoBehaviour {

    [Header("Components")]
    private Rigidbody2D _RB2D = null;
    private GameObject _target = null;
    public GameObject _player = null;
    public GameObject _left = null;
    public GameObject _right = null;

    [Header("Horizontal movement")]
    [SerializeField] private float horizontalEpsilon = 0.5f;
    [SerializeField] private float enemySpeed = 3.5f;

    [Header("Vertical movement")]
    [SerializeField] private LayerMask triggerMasks;
    [SerializeField] private float verticalEpsilon = 1f;
    [SerializeField] private float jumpForce = 15f;
    [SerializeField] private bool inJumpTrigger = false;
    [SerializeField] private float jumpCooldown = 0.25f;
    [SerializeField] private float jumpCooldownCounter = 0f;

    [Header("Ground Ray Settings")]
    [SerializeField] private bool isGrounded = false;
    [SerializeField] private float groundRayLength = 0.34f;
    [SerializeField] private Vector3 groundRaysCenterOffset;
    [SerializeField] private LayerMask groundLayers;

    [Header("Nav Settings")]
    [SerializeField] private bool hasTarget = false;
    [SerializeField] private bool inBottomTrigger = false;
    [SerializeField] private bool inTopTrigger = false;


    private void Awake() {
        _RB2D = GetComponent<Rigidbody2D>();
        _player = GameObject.Find("Player");
        _left = GameObject.Find("Left Target");
        _right = GameObject.Find("Right Target");
        enemySpeed *= Random.Range(0.9f, 1.1f); 
    }

    private void Start() {
        _target = _player;
    }

    private void Update() {
        if (_RB2D.velocity.x > 0f) { // change sprite orientation
            transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        } else {
            transform.rotation = Quaternion.Euler(0f, 180f, 0f);
        }

        groundCheck();
        if (isGrounded) { 
            jumpCooldownCounter += Time.deltaTime;
        }

        if (!hasTarget) {
            setTarget();
        }
    }

    private void FixedUpdate() {
        if (_target) {
            moveHorizontal();
            moveVertical();
        }
    }
    private void setTarget() {
        if (inBottomTrigger && _player.GetComponent<playerManager>().inTopTrigger || // If the enemy is on the bottom and player on top, switch targets to one of the sides
            inTopTrigger && _player.GetComponent<playerManager>().inBottomTrigger) { // If the enemy is on the top and player on bottom, switch targets to one of the sides
            float rng = Random.value;

             
            if (Mathf.Abs(_RB2D.position.x - _left.transform.position.x) < Mathf.Abs(_RB2D.position.x - _right.transform.position.x)) { // choose target according to the distance from it
                _target = _left;
            } else {
                _target = _right;
            }

            if (rng > 0.9) { // With a 10% chance, stay on your platform and mirror the player movement without jumping
                _target = _player;
            }
        } else { // We are on the same plane or are reachable
            _target = _player;
        }

        hasTarget = true;
    }

    private void moveHorizontal() {
        if (_target.transform.position.x - transform.position.x > horizontalEpsilon) { // Target is to the right of the enemy, move right
            _RB2D.velocity = new Vector2(enemySpeed, _RB2D.velocity.y);
        } else if (_target.transform.position.x - transform.position.x < -horizontalEpsilon) { // Target is to the left of the enemy, move left
            _RB2D.velocity = new Vector2(-enemySpeed, _RB2D.velocity.y);
        } else { // Dont move
            _RB2D.velocity = new Vector2(Mathf.MoveTowards(_RB2D.velocity.x, 0, 1f), _RB2D.velocity.y);
            hasTarget = false;
        }
    }

    private void moveVertical() {
        // Going up
        if (_target.transform.position.y - transform.position.y > verticalEpsilon) { // platforms above
            if (isGrounded && inJumpTrigger) {
                if (jumpCooldown < jumpCooldownCounter) {
                    jump();
                }
            }
        } else if (_target.transform.position.y - transform.position.y < -verticalEpsilon) { // platforms bellow
            // For pacing reasons, the "ducks" are unable to pass down through platforms like player
        } else { // same platform
            
        }
    }

    private void jump() {
        _RB2D.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        jumpCooldownCounter = 0;
    }

    private void groundCheck() {
        isGrounded = Physics2D.Raycast(transform.position + groundRaysCenterOffset, Vector2.down, groundRayLength, groundLayers) ||
                        Physics2D.Raycast(transform.position - groundRaysCenterOffset, Vector2.down, groundRayLength, groundLayers);
    }

    private void OnDrawGizmos() {
        Gizmos.DrawLine(transform.position + groundRaysCenterOffset, transform.position + groundRaysCenterOffset + Vector3.down * groundRayLength);
        Gizmos.DrawLine(transform.position - groundRaysCenterOffset, transform.position - groundRaysCenterOffset + Vector3.down * groundRayLength);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.layer == LayerMask.NameToLayer("JumpTriggers")) {
            inJumpTrigger = true;
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("TopTrigger")) {
            inTopTrigger = true;
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("BottomTrigger")) {
            inBottomTrigger = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.layer == LayerMask.NameToLayer("JumpTriggers")) {
            inJumpTrigger = false;
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("TopTrigger")) {
            inTopTrigger = false;
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("BottomTrigger")) {
            inBottomTrigger = false;
        } 
    }
}
