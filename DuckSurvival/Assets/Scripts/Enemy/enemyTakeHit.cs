using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class enemyTakeHit : MonoBehaviour {
    [Header("Components")]
    private Rigidbody2D _RB2D = null;
    private bool isTakingHit = false;
    private float timer = 0f;
    [SerializeField] private float afterHitTimer = 0.6f;

    private void Awake() {
        _RB2D = GetComponent<Rigidbody2D>(); 
    }

    private void Update() {
        if (isTakingHit) {
            timer += Time.deltaTime;
            GetComponent<enemyFollowTarget>().enabled = false;
        }

        if (timer > afterHitTimer) {
            isTakingHit = false;
            timer = 0f;
            GetComponent<enemyFollowTarget>().enabled = true;
        }
    }

    public void takeHit(Vector3 playerPosition, float force, float liftForce) {
        isTakingHit = true;
        pushBack(playerPosition, force, liftForce);
    }

    private void pushBack(Vector3 playerPosition, float force, float liftForce) {
        Vector3 direction = transform.position - playerPosition;

        _RB2D.AddForce(direction.normalized * force, ForceMode2D.Impulse);
        _RB2D.AddForce(Vector2.up * liftForce, ForceMode2D.Impulse);
    }
}
