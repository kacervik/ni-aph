using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyDie : MonoBehaviour {
    [SerializeField] private bool inGameBounds = true;
    [SerializeField] private float explosionRadius = 1f;
    [SerializeField] private float explosionStrength = 10f;
    [SerializeField] private LayerMask layerMask;

    private void FixedUpdate() {
        if (!inGameBounds) {
            Destroy(gameObject);
        }
    }

    private void OnDestroy() {
        Debug.Log("Enemy Destroyed");
        explode();
        // hitPause.instance.Pause(0.2f);
        CameraShake.instance.ShakeCamera(0.1f, 0.8f * UIManager.instance.getExplosionSusceptibility());
        // Add particles and camera shake
    }

    private void explode() {
        Collider2D[] explosionCollider;
        explosionCollider = Physics2D.OverlapCircleAll(transform.position, explosionRadius, layerMask);

        foreach (Collider2D player in explosionCollider) {
            if (player != null) {
                Vector2 distance = player.transform.position - transform.position;
                if (distance.magnitude > 0) {
                    player.GetComponent<Rigidbody2D>().AddForce(distance.normalized * (explosionStrength/distance.magnitude) * UIManager.instance.getExplosionSusceptibility());
                    UIManager.instance.increaseExplosionSusceptibility();
                    break; // we have a single player, so no need to check all the ducks
                }
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.layer == LayerMask.NameToLayer("GameBounds")) {
            inGameBounds = true;
        }

        
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.layer == LayerMask.NameToLayer("GameBounds")) {
            inGameBounds = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player")) {
            Debug.Log("touching player");
            Destroy(gameObject);
        }
    }

    private void OnDrawGizmos() {
        Gizmos.DrawWireSphere(transform.position, explosionRadius);
    }
}
