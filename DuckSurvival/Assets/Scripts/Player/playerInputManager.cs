using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class playerInputManager : MonoBehaviour {
    public Vector2 inputVector = Vector2.zero;
    public bool jumpButton = false;
    public bool intendedJump = false;
    public bool attackButton = false;
    public bool pauseButton = false;

    private IA_CharacterInput _input = null;

    private void Awake() {
        _input = new IA_CharacterInput();
    }

    // Input management
    private void OnEnable() {
        _input.Enable();
        _input.Player.Move.performed += OnMovement;
        _input.Player.Move.canceled += OnMovement;
    }

    private void OnDisable() {
        _input.Disable();
        _input.Player.Move.performed -= OnMovement;
        _input.Player.Move.canceled -= OnMovement;
    }

    // Getting input 
    public void OnMovement(InputAction.CallbackContext context) {
        inputVector = context.ReadValue<Vector2>();
    }

    public void OnJump(InputAction.CallbackContext context) {
        if (context.started) {
            intendedJump = true;
            jumpButton = true;
        }

        if (context.canceled) {
            jumpButton = false;
        }
    }

    public void OnAttack(InputAction.CallbackContext context) {
        if (context.started) {
            attackButton = true;
        }

        if (context.canceled) {
            attackButton = false;
        }
    }

    public void OnPause(InputAction.CallbackContext context) {
        if (context.performed) {
            pauseButton = !pauseButton;
        }
    }
}
