using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class playerManager : MonoBehaviour {
    // AI navigation
    [Header("AI")]
    public bool inTopTrigger = false;
    public bool inBottomTrigger = false;

    [Header("Spawning and health")]
    [SerializeField] private Vector2 spawnPoint = new Vector3(0f, 10f, 0f);
    public int lives = 3;
    [SerializeField] private bool inGameBounds = true;
    public bool isRespawning = false;

    private void FixedUpdate() {
        if (!isRespawning) {
            if (!inGameBounds) { // the player left the playable area, we need to respawn
                Debug.Log("Out of bounds");
                if (lives > 0) {
                        Debug.Log("Respawning initiated");
                        StartCoroutine(Respawn());
                } else {
                    // GAME OVER
                    Debug.Log("Game Over");
                    GameOverMenu.instance.gameOver();
                }
            }
        }
    }

    private IEnumerator Respawn() {
        isRespawning = true;
        Debug.Log("Respawning...");

        // hitPause.instance.Pause(0.2f);
        CameraShake.instance.ShakeCamera(0.25f, 1.8f);
        lives--;
        UIManager.instance.decreaseLives();
        yield return new WaitForSeconds(2f);

        transform.localScale = Vector3.zero;
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        transform.localScale = Vector3.one;
        transform.position = spawnPoint;

        yield return null;

        UIManager.instance.resetExplosionSusceptibility();

        Debug.Log("Respawn completed");
        inGameBounds = true;
        isRespawning = false;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.layer == LayerMask.NameToLayer("TopTrigger")) {
            inTopTrigger = true;
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("BottomTrigger")) {
            inBottomTrigger = true;
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("GameBounds")) {
            inGameBounds = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.layer == LayerMask.NameToLayer("TopTrigger")) {
            inTopTrigger = false;
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("BottomTrigger")) {
            inBottomTrigger = false;
        }

        if (collision.gameObject.layer == LayerMask.NameToLayer("GameBounds")) {
            inGameBounds = false;
        }
    }
}
