using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
// using UnityEditor.Rendering;
// using UnityEditor.U2D.Animation;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;
// using static UnityEditor.Timeline.TimelinePlaybackControls;

public class playerHorizontalMovement : MonoBehaviour {

    [Header("Components")]
    private Rigidbody2D _RB2D = null;
    private playerInputManager _input = null;
    // private playerGroundChecks _groundCheck = null;
    private Animator _anim = null;
    // private ParticleSystem _particleSystem = null;

    [Header("Movement Settings")]
    [SerializeField] private float maxSpeed = 5.0f;
    [SerializeField] private float acceleration = 1.0f;
    [SerializeField] private float deceleration = 1.0f;
    [SerializeField] private float maxSpeedChange = 1.0f;
    [SerializeField] private float turnSpeed = 1.0f;

    [Header("Movement Visualization")]
    [SerializeField] private float instantVelocity = 0.0f;
    [SerializeField] private Vector2 velocity;
    [SerializeField] private bool isHoldingKey = false;



    // Get necessary components
    private void Awake() {
        _RB2D = GetComponent<Rigidbody2D>();
        _input = GetComponent<playerInputManager>();
        // _groundCheck = GetComponent<playerGroundChecks>();
        _anim = GetComponentInChildren<Animator>();
        // _particleSystem = GetComponentInChildren<ParticleSystem>();
    }

    private void Update() {
        if (_input.inputVector.x != 0) {
            isHoldingKey = true;
            transform.localScale = new Vector3(_input.inputVector.x > 0 ? 1f : -1f, 1f, 1f);
        } else {
            isHoldingKey = false;
            _anim.SetBool("isRunning", false);
        }

        // if (isHoldingKey && _groundCheck.isGrounded) {
        //     Debug.Log("Particles start");
        //     _particleSystem.Play();
        // } else {
        //     Debug.Log("Particles stop");
        //     _particleSystem.Stop();
        // }
    }

    // Updates every physics tick
    private void FixedUpdate() {
        if (!GetComponent<playerAttack>().isChargingHeavyAttack) {
            _anim.SetBool("isRunning", true);
            acceleratedMovement(); 
        } else {
        }
    }

    private void acceleratedMovement() {
        velocity = _RB2D.velocity;

        instantVelocity = _input.inputVector.x * maxSpeed;

        if (isHoldingKey) {
            if (Mathf.Sign(_input.inputVector.x) != Mathf.Sign(velocity.x)) {
                maxSpeedChange = turnSpeed * Time.deltaTime;
            } else {
                maxSpeedChange = acceleration * Time.deltaTime;
            }
        } else {
            maxSpeedChange = deceleration * Time.deltaTime;
        }

        velocity.x = Mathf.MoveTowards(velocity.x, instantVelocity, maxSpeedChange);

        _RB2D.velocity = velocity;
    }
}
