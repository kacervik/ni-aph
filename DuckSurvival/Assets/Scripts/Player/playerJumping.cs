using System.Collections;
using System.Collections.Generic;
// using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;
// using static UnityEditor.Timeline.TimelinePlaybackControls;

public class playerJumping : MonoBehaviour {
    [Header("Components")]
    private Rigidbody2D _RB2D = null;
    private playerInputManager _input = null;
    private playerGroundChecks _groundCheck = null;
    private SFXPlayer _SFXplayer;
    private Animator _anim;

    [Header("Jump Bools")]
    // [SerializeField] private bool intendedJump = false;
    [SerializeField] private bool isJumping = false;

    [Header("Jump Settings")]
    [SerializeField] private float jumpHeight = 3f;
    [SerializeField] private float jumpApexTime = 0.4f;
    [SerializeField] private float wallJumpStrength = 30f;

    [Header("Multiple Jump")]
    [SerializeField] private int maxAirJumps = 1;
    [SerializeField] private int airJumpCounter = 0;

    [Header("Gravity Settings")]
    [SerializeField] private float defaultGravity = 1f;
    [SerializeField] private float upwardGravityMultiplier = 2f;
    [SerializeField] private float downwardGravityMultiplier = 3f;
    [SerializeField] private Vector2 newGravity;

    [Header("Jump Safetynet")]
    [SerializeField] private float jumpBuffer = 0.2f; // How much time do we allow to press jump before touching ground
    [SerializeField] private float coyoteJump = 0.2f; // How much time do we allow to press jump after touching ground

    [Header("Jumping Calculations")]
    [SerializeField] private Vector2 velocity = Vector2.zero;
    [SerializeField] private float jumpForce;
    [SerializeField] private float newGravityMultiplier ;
    [SerializeField] private float jumpBufferCounter;
    [SerializeField] private float coyoteTimeCounter;


    private float verticalVelocityEpsilon = 0.01f;

    // Get necessary components
    private void Awake() {
        _RB2D = GetComponent<Rigidbody2D>();
        _input = GetComponent<playerInputManager>();
        _groundCheck = GetComponent<playerGroundChecks>();
        _SFXplayer = GetComponentInChildren<SFXPlayer>();
        _anim = GetComponentInChildren<Animator>();
    }

    private void Update() {
        if (jumpBuffer > 0) { // if we have jump buffer set up
            if (jumpBufferCounter > jumpBuffer) { // check if the counter is over our buffer and then set false
                _input.intendedJump = false;
                jumpBufferCounter = 0;
            }

            if (_input.intendedJump) { // start timer if we pressed jump button
                jumpBufferCounter += Time.deltaTime;

            }
        }

        // If we are not grounded and arent jumping (fell from platform)
        // start timer and check in jump method
        if (!_groundCheck.isGrounded && !isJumping) {
            coyoteTimeCounter += Time.deltaTime;
        } else {
            coyoteTimeCounter = 0;
        }

        // When grounded
        // reset "double" jump
        if (_groundCheck.isGrounded) {
            airJumpCounter = 0;
        }

        if (!_groundCheck.isGrounded) {
            if (_groundCheck.isOnWall) {
                if (shouldSlideOnWall()) {
                    slideOnWall();
                }

                if (_input.intendedJump) {
                    wallJump();
                }
            }
        }

        if (_groundCheck.isOnWall) {
            _anim.SetBool("isOnWall", true);
        } else {
            _anim.SetBool("isOnWall", false);
        }
    }

    // Update is called once per frame
    private void FixedUpdate() {
        if (!GetComponent<playerAttack>().isChargingHeavyAttack) {
            changeGravity();
            setGravity();
            CalculateJumpForce();

            if (_input.intendedJump) {
                jump();
                return; // This is here to prevent double jump bug (according to some youtube video)
            }
        } 
    }

    private void setGravity() { // some physics formula for calculating gravity
        newGravity = new Vector2(0, (-2 * jumpHeight) / (jumpApexTime * jumpApexTime));
        _RB2D.gravityScale = (newGravity.y / Physics2D.gravity.y) * newGravityMultiplier;
    }

    private void changeGravity() {
        if (_RB2D.velocity.y > verticalVelocityEpsilon) {
            newGravityMultiplier = upwardGravityMultiplier;
        } else if (_RB2D.velocity.y < -verticalVelocityEpsilon) {
            newGravityMultiplier = downwardGravityMultiplier;
        } else {
            if (_groundCheck.isGrounded) {
                isJumping = false;
            }
            newGravityMultiplier = defaultGravity;
        }
    }

    private void CalculateJumpForce() {
        jumpForce = Mathf.Sqrt(-2f * Physics2D.gravity.y * (newGravity.y / Physics2D.gravity.y) * upwardGravityMultiplier * jumpHeight);
    }

    private void jump() {
        if (_groundCheck.isGrounded || (airJumpCounter < maxAirJumps) || (coyoteTimeCounter < coyoteJump && coyoteTimeCounter > 0.03f)) {
            _SFXplayer.playJumpSFX();
            CameraShake.instance.ShakeCamera(0.1f, 0.2f);
            _input.intendedJump = false;
            jumpBufferCounter = 0;
            coyoteTimeCounter = 0;

            _RB2D.velocity = new Vector2(_RB2D.velocity.x, jumpForce);

            airJumpCounter++;
            isJumping = true;
        }
    }

    private void wallJump() {
        _SFXplayer.playJumpSFX(2f);
        CameraShake.instance.ShakeCamera(0.1f, 0.4f);
        Debug.Log("Wall Jump");
        _input.intendedJump = false;
        jumpBufferCounter = 0;
        coyoteTimeCounter = 0;

        CalculateJumpForce();
        if (_groundCheck.isOnLeftWall) {
            _RB2D.AddForce(new Vector2(0.707f, 0.707f) * wallJumpStrength, ForceMode2D.Impulse);
        } else {
            _RB2D.AddForce(new Vector2(-0.707f, 0.707f) * wallJumpStrength, ForceMode2D.Impulse);
        }

        isJumping = true;
        // _anim.SetBool("isOnWall", false);
    }

    private bool shouldSlideOnWall() {
        return _RB2D.velocity.y < -verticalVelocityEpsilon &&
                    ((_groundCheck.isOnWall && _groundCheck.isOnLeftWall && _input.inputVector.x < 0f) ||
                     (_groundCheck.isOnWall && !_groundCheck.isOnLeftWall && _input.inputVector.x > 0f));
    }

    private void slideOnWall() {
        // _anim.SetBool("isOnWall", true);
        velocity = _RB2D.velocity;
        velocity.y = 0f;
        _RB2D.velocity = velocity;
    }
}
