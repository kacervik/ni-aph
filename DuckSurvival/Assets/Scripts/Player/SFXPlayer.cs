using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXPlayer : MonoBehaviour {
    [Header("SFX")]
    public AudioClip footstepSFX;
    public AudioClip attackLightSFX;
    public AudioClip attackHeavySFX;
    public AudioClip jumpSFX;

    [Header("Source")]
    public AudioSource audioSource;


    private void Start() {
        audioSource = GetComponent<AudioSource>();
    }

    public void playFootstepSFX(float volume = 1.0f) {
        playSound(footstepSFX, volume);
    }

    public void playAttackLightSFX(float volume = 1.0f) {  
        playSound(attackLightSFX, volume);
    }

    public void playAttackHeavySFX(float volume = 1.0f) { 
        playSound(attackHeavySFX, volume);
    }

    public void playJumpSFX(float volume = 1.0f) { 
        playSound(jumpSFX, volume);
    }


    private void playSound(AudioClip sound, float volume = 1.0f, float pitchRange = 0.05f) {
        if (sound != null) {
            audioSource.clip = sound;

            // Set the pitch to a random value within the specified range
            float randomPitch = Random.Range(1.0f - pitchRange, 1.0f + pitchRange);
            audioSource.pitch = Mathf.Clamp(randomPitch, 0.5f, 2.0f); // Ensure pitch is within valid range

            audioSource.volume = Mathf.Clamp01(volume); // Ensure volume is within valid range
            audioSource.Play();
        }
    }
}
