using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerGroundChecks : MonoBehaviour {
    [Header("Grounded")]
    public bool isGrounded = false;
    public bool isOnWall = false;
    public bool isOnLeftWall = false;

    [Header("Ground Ray Settings")]
    [SerializeField] private float groundRayLength = 1.0f;
    [SerializeField] private Vector3 groundRaysCenterOffset;
    [SerializeField] private LayerMask groundLayers;

    [Header("Wall Ray Settings")]
    [SerializeField] private float wallRayLength = 1.0f;
    [SerializeField] private Vector3 wallRaysCenterOffset;
    [SerializeField] private LayerMask wallLayers;

    private void Update() {
        groundCheck();
        WallCheck();
    }

    private void groundCheck() {
        isGrounded = Physics2D.Raycast(transform.position + groundRaysCenterOffset, Vector2.down, groundRayLength, groundLayers) ||
                        Physics2D.Raycast(transform.position - groundRaysCenterOffset, Vector2.down, groundRayLength, groundLayers);
    }

    private void WallCheck() {
        isOnWall = Physics2D.Raycast(transform.position + wallRaysCenterOffset, Vector2.right, wallRayLength, wallLayers) ||
                    Physics2D.Raycast(transform.position + wallRaysCenterOffset, Vector2.left, wallRayLength, wallLayers);
        isOnLeftWall = Physics2D.Raycast(transform.position + wallRaysCenterOffset, Vector2.left, wallRayLength, wallLayers);

    }

    private void OnDrawGizmos() {
        if (isGrounded) { 
            Gizmos.color = Color.green; 
        } else { 
            Gizmos.color = Color.red; 
        }

        Gizmos.DrawLine(transform.position + groundRaysCenterOffset, transform.position + groundRaysCenterOffset + Vector3.down * groundRayLength);
        Gizmos.DrawLine(transform.position - groundRaysCenterOffset, transform.position - groundRaysCenterOffset + Vector3.down * groundRayLength);

        Gizmos.DrawLine(transform.position + wallRaysCenterOffset, transform.position + wallRaysCenterOffset + Vector3.right * wallRayLength);
        Gizmos.DrawLine(transform.position + wallRaysCenterOffset, transform.position + wallRaysCenterOffset + Vector3.left * wallRayLength);
    }
}
