using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class playerOneWayPlatformPassthrough : MonoBehaviour {

    private GameObject currentOWP;
    private playerInputManager _input = null;
    [SerializeField] private CapsuleCollider2D playerCollider;
    [SerializeField] private float disableTime = 0.2f;


    // Start is called before the first frame update
    void Awake() {
        _input = GetComponent<playerInputManager>();
        playerCollider = GetComponent<CapsuleCollider2D>();
    }

    private void Update() {
        if (_input.inputVector.y < -0.5f) { 
            if (currentOWP != null) {
                StartCoroutine(DisableCollision());
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.layer == LayerMask.NameToLayer("OneWayPlatform")) {
            currentOWP = collision.gameObject;
        }
    }

    private void OnCollisionExit2D(Collision2D collision) {
        if (collision.gameObject.layer == LayerMask.NameToLayer("OneWayPlatform")) {
            currentOWP = null;
        }
    }

    private IEnumerator DisableCollision() {
        BoxCollider2D platformCollider = currentOWP.GetComponent<BoxCollider2D>();

        Physics2D.IgnoreCollision(playerCollider, platformCollider);
        yield return new WaitForSeconds(disableTime);
        Physics2D.IgnoreCollision(playerCollider, platformCollider, false);
    }
}
