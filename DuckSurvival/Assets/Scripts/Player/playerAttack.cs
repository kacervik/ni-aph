using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class playerAttack : MonoBehaviour {

    [Header("Components")]
    private Rigidbody2D _RB2D = null;
    private playerInputManager _input = null;
    private playerGroundChecks _groundCheck = null;
    [SerializeField] private GameObject slashEffect = null;
    private SFXPlayer _SFXplayer;
    private Animator _anim = null;
    // [SerializeField] private CameraShake cameraShake;

    [Header("Attack range properties")]
    [SerializeField] private float attackRangeX = 1f;
    [SerializeField] private float attackRangeY = 1f;
    [SerializeField] private float attackAngle = 0f;
    [SerializeField] private float heavyAttackTreshold = 0.15f;
    [SerializeField] private float heavyAttackUpperTreshold = 0.8f;

    [Header("Light Attack Properties")]
    [SerializeField] private float lightAttackStrength = 8f;
    [SerializeField] private float lightAttackUpliftForce = 6f;
    [SerializeField] private float lightAttackRecoil = 3f;

    [Header("Heavy Attack Properties")]
    [SerializeField] private float heavyAttackStrength = 17f;
    [SerializeField] private float heavyAttackUpliftForce = 10f;
    [SerializeField] private float heavyAttackRecoil = 5f;
    [SerializeField] public bool isChargingHeavyAttack = false;

    [Header("Attack position")]
    [SerializeField] Vector3 centerPos;
    [SerializeField] private Vector2 sidePosition;
    [SerializeField] private Vector2 verticalPosition;

    [Header("Collision properties")]
    [SerializeField] private Transform attackTransform = null;
    [SerializeField] private LayerMask enemyLayers;

    [Header("Calculations")]
    [SerializeField] private float attackTypeTimer = 0f;
    [SerializeField] private float timerEpsilon = 0.01f;
    [SerializeField] private float attackCooldown = 0.33f;
    [SerializeField] private float attackCooldownTimer = 0f;

    private void Awake() {
        _RB2D = GetComponent<Rigidbody2D>();
        _input = GetComponent<playerInputManager>();
        _groundCheck = GetComponent<playerGroundChecks>();
        centerPos = attackTransform.position;
        _SFXplayer = GetComponentInChildren<SFXPlayer>();
        _anim = GetComponentInChildren<Animator>();
    }

    private void Update() {
        changeAttackTransform();
        getAttackType();

        if (isChargingHeavyAttack) {
            _anim.SetBool("isRunning", false);
            CameraShake.instance.ShakeCamera(0.1f, 0.8f*attackTypeTimer);
        }

        attackCooldownTimer += Time.deltaTime;
    }

    private void FixedUpdate() {
        freezeMovement();        
    }

    private void freezeMovement() {
        if (isChargingHeavyAttack) {
            _RB2D.velocity = Vector2.zero;
            _RB2D.gravityScale = 0f;
        }  
    }

    private void getAttackType() {
        if (_input.attackButton) {
            // Count for how long we are pressing the attack button.
            attackTypeTimer += Time.deltaTime;
        }

        if (attackTypeTimer > heavyAttackTreshold) {
            // If we are holding the button abouve a treshold, we are charging the heavy attack
            isChargingHeavyAttack = true;
        }
        
        if (attackTypeTimer > heavyAttackUpperTreshold) {
            // We wont allow player to charge heavy attack forever
            isChargingHeavyAttack = false;
            meleeAttack(true);
            attackTypeTimer = 0f;
        }

        if (attackTypeTimer <= timerEpsilon) {
            // to fix a bug where timer got reset, but the charging was still true
            isChargingHeavyAttack = false;
        }

        if (!_input.attackButton) {
            if (attackTypeTimer > heavyAttackTreshold && attackTypeTimer < heavyAttackUpperTreshold) {
                Debug.Log("Heavy Attack");
                meleeAttack(true);
                isChargingHeavyAttack = false;
            } else if (attackTypeTimer > timerEpsilon) {
                Debug.Log("Light Attack");
                meleeAttack(false);
            } 
            attackTypeTimer = 0f;
        }
    }

    private void changeAttackTransform() {
        // Change attack position and rotation
        Vector2 playerPosition2D = new Vector2(transform.position.x, transform.position.y);
        if (_input.inputVector.y > 0) {
            // above
            attackTransform.position = playerPosition2D + verticalPosition;
            if (transform.localScale.x > 0) {
                attackTransform.rotation = Quaternion.Euler(0, 0, +90f);    // Works only for sprites, not for the bounding box of attack, and makes sure the sprite is in the right orientation
            } else {
                attackTransform.rotation = Quaternion.Euler(0, 0, -90f);    
            }
            attackAngle = -90f;
        } else if (_input.inputVector.y < 0) {
            // bellow
            attackTransform.position = playerPosition2D + (-verticalPosition);
            // attackTransform.rotation = Quaternion.Euler(0, 0, -90f);    // Works only for sprites, not for the bounding box of attack
            if (transform.localScale.x > 0) {
                attackTransform.rotation = Quaternion.Euler(0, 0, -90f);    
            } else {
                attackTransform.rotation = Quaternion.Euler(0, 0, +90f);    
            }
            attackAngle = -90f;
        } else {
            if (transform.localScale.x > 0) {
                attackTransform.position = playerPosition2D + sidePosition;
            } else {
                attackTransform.position = playerPosition2D - sidePosition;
            }
            attackTransform.rotation = Quaternion.Euler(0, 0, 0);   // Works only for sprites, not for the bounding box of attack
            attackAngle = 0f;
        }

        centerPos = attackTransform.position;
    }

    private void meleeAttack(bool heavy = false) {
        if (attackCooldown <= attackCooldownTimer) {
            attackCooldownTimer = 0f;
            Debug.Log("Melee Attack, heavy = " + heavy);
            Collider2D[] enemiesHit;
            enemiesHit = Physics2D.OverlapBoxAll(centerPos, new Vector2(attackRangeX, attackRangeY), attackAngle, enemyLayers);

            // SLASH ANIMATION
            Instantiate(slashEffect, attackTransform);

            foreach (Collider2D enemy in enemiesHit) {
                if (enemy != null) {
                    // FindObjectOfType<hitPause>().Pause(1f);
                    // Debug.Log(enemy.name);
                    if (heavy) {
                        heavyAttackStrength = heavyAttackStrength + (1f + attackTypeTimer); // stronger the longer we hold
                        enemy.GetComponent<enemyTakeHit>().takeHit(transform.position, heavyAttackStrength, heavyAttackUpliftForce);
                    } else {
                        enemy.GetComponent<enemyTakeHit>().takeHit(transform.position, lightAttackStrength, lightAttackUpliftForce);
                    }
                }
            }

            if (heavy) {
                _SFXplayer.playAttackHeavySFX();
                hitPause.instance.Pause(0.08f);
                CameraShake.instance.ShakeCamera(0.08f, 1.2f);
                pushBack(centerPos, heavyAttackRecoil);
                if (!_groundCheck.isGrounded && !_groundCheck.isOnWall) {
                    _RB2D.AddForce(Vector2.up * 20f, ForceMode2D.Impulse);
                }
            } else {
                _SFXplayer.playAttackLightSFX();
                hitPause.instance.Pause(0.03f);
                CameraShake.instance.ShakeCamera(0.1f, 0.3f);
                pushBack(centerPos, lightAttackRecoil);
            }
        }
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        Vector3 centerPos = attackTransform.position;
        Gizmos.DrawWireCube(centerPos, new Vector3(attackRangeX, attackRangeY, 0f));
    }

    private void pushBack(Vector3 origin, float force) {
        //Vector2 playerPosition2D = new Vector2(transform.position.x, transform.position.y);
        Vector3 direction = transform.position - origin;
        _RB2D.AddForce(direction.normalized * force, ForceMode2D.Impulse);
    }
}
