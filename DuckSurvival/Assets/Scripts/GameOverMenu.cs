using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour {
    public static GameOverMenu instance;
    public GameObject gameOverPanel;
    public GameObject gameOverFirstButton;

    private void Awake() {
        instance = this;
    }

    public void gameOver() {
        Time.timeScale = 0;
        gameOverPanel.SetActive(true);
        setGameOverFirstButton();
    }

    public void restartGame() {
        Time.timeScale = 1f;
        gameOverPanel.SetActive(false);
        SceneManager.LoadSceneAsync(1);
    }

    public void returnToMainMenu() {
        Time.timeScale = 1f;
        gameOverPanel.SetActive(false);
        SceneManager.LoadSceneAsync(0);
    }
    public void setGameOverFirstButton() {
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(gameOverFirstButton);
    }
}
