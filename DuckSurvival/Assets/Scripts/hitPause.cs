using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class hitPause : MonoBehaviour {

    public static hitPause instance;

    bool isWaiting;

    private void Awake() {
        instance = this;
    }

    public void Pause(float duration) {
        Debug.Log("PAUSE");

        if (isWaiting) {
            return;
        }

        Time.timeScale = 0.0f;
        StartCoroutine(Wait(duration));
    }

    IEnumerator Wait(float duration) {
        Debug.Log("PAUSED");
        isWaiting = true;
        yield return new WaitForSecondsRealtime(duration);
        Time.timeScale = 1.0f;
        isWaiting = false;
    }
}
