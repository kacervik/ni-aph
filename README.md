# Duck Survival

Itch.io link: https://huntahsvk.itch.io/duck-survival

- *Viktor Káčer*
- **Genre**: Platformer survival game
- **Engine**: Unity (2022.3.12f1)
- **Space**: 2D Static Scene
- **Objects**: Characters, Enemies, Platforms
- **Actions**:
    - movement
    - light/heavy attack
- **Rules**:
    - Movement
        - double jump resets on ground touch
    - Choosing direction of attacks
        - heavy attack resets on ground touch
    - Loosing lives when falling out of map
    - Enemies explode on touch
        - explosions cause pushback
    - Explosion susceptibility bar
        - fills up by getting hit by explosions
        - fuller the meter, character reacts more to explosion pushback
- **Game Objective**
    - Surviving as long as possible
- **Main mechanic**/Technical Challenge
    - snappy precise movement with attack selection depending on joystick direction
    - enemy AI/explosion susceptibility bar/balanced wave system

*(idea came from Gun Mayhem gamemode and mechanics from Brawlhalla)*